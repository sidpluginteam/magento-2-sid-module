<?php
namespace SID\InstantEFT\Controller\Redirect;

class Index extends \SID\InstantEFT\Controller\AbstractSID
{
    protected $resultPageFactory;

    public function execute() {
        try {
            $data = $this->getRequest()->getParams();
            $this->_logger->debug( __METHOD__ . ' : ' . print_r($data, true));
            $payment_successful = false;
            if( $this->_sidResponseHandler->validateResponse($data) ) {
                if( $this->_sidResponseHandler->checkResponseAgainstSIDWebQueryService($data, $this->_date->gmtDate()) ) {
                    $payment_successful = true;
                    $this->_redirect('checkout/onepage/success');
                }
            }
            if(!$payment_successful){
                throw new \Magento\Framework\Exception\LocalizedException(__('Your payment was unsuccessful'));
            }
        } catch ( \Magento\Framework\Exception\LocalizedException $e ) {
            $this->_logger->debug( __METHOD__ . ' : ' . $e->getMessage());
            $this->messageManager->addExceptionMessage( $e, $e->getMessage() );
            $this->_redirect( 'checkout/cart' );
        } catch ( \Exception $e ) {
            $this->_logger->debug( __METHOD__ . ' : ' . $e->getMessage() . '\n' . $e->getTraceAsString());
            $this->messageManager->addExceptionMessage( $e, __( 'We can\'t start SID Checkout.' ) );
            $this->_redirect( 'checkout/cart' );
        }
    }

}