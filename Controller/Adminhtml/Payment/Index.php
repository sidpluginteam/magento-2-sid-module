<?php
namespace SID\InstantEFT\Controller\Adminhtml\Payment;

use Magento\Framework\Controller\ResultFactory;

class Index extends \SID\InstantEFT\Controller\Adminhtml\Payment
{
    public function execute()
    {
        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(__('Search Payments'), __('Search Payments'));
        $resultPage->getConfig()->getTitle()->prepend(__('SID Instant EFT Payments'));
        return $resultPage;
    }
}