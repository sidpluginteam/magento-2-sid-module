<?php
namespace SID\InstantEFT\Controller\Notify;

class Index extends \SID\InstantEFT\Controller\AbstractSID
{
    public function execute() {
        try {
            $this->_logger->debug( __METHOD__ . ' : ' . print_r($_POST, true));
            if ($this->_sidResponseHandler->validateResponse($_POST)) {
                if($this->_sidResponseHandler->checkResponseAgainstSIDWebQueryService($_POST, null, $this->_date->gmtDate())) {
                    $this->_logger->debug( __METHOD__ . ' : Payment Successful');
                } else {
                    $this->_logger->debug( __METHOD__ . ' : Payment Unsuccessful');
                }
            };
            header('HTTP/1.0 200 OK');
            flush();
        } catch ( \Exception $e ) {
            $this->_logger->debug( __METHOD__ . ' : ' . $e->getMessage() . '\n' . $e->getTraceAsString());
            $this->messageManager->addExceptionMessage( $e, __( 'We can\'t start SID Checkout.' ) );
            $this->_redirect( 'checkout/cart' );
        }
    }
}