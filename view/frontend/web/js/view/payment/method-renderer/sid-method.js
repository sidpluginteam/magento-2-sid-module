define(['jquery', 'Magento_Checkout/js/view/payment/default', 'Magento_Checkout/js/action/place-order',
    'Magento_Checkout/js/action/select-payment-method', 'Magento_Customer/js/model/customer', 'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/payment/additional-validators', 'mage/url'
    ],
    function ($, Component, placeOrderAction, selectPaymentMethodAction, customer, checkoutData, additionalValidators, url)  {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'SID_InstantEFT/payment/sid'
            },
            getCode: function() {
                return 'sid';
            },
            getInstructions: function () {
                return window.checkoutConfig.payment.instructions[this.item.method];
            },
            isAvailable: function() {
                return quote.totals().grand_total <= 0;
            },
            afterPlaceOrder: function () {
                window.location.replace( url.build(window.checkoutConfig.payment.sid.redirectUrl.sid) );
            },
            getPaymentAcceptanceMarkHref: function() {
                return window.checkoutConfig.payment.sid.paymentAcceptanceMarkHref;
            },
            getPaymentAcceptanceMarkSrc: function() {
                return window.checkoutConfig.payment.sid.paymentAcceptanceMarkSrc;
            }
        });
    });