define(['uiComponent', 'Magento_Checkout/js/model/payment/renderer-list'],
    function (Component, rendererList) {
        'use strict';
        rendererList.push({
            type: 'sid',
            component: 'SID_InstantEFT/js/view/payment/method-renderer/sid-method'
        });
        return Component.extend({});
    });