# SID Instant EFT Payment Module for Magento 2

This extension utilizes SID Instant EFT Payment Gateway and provides seamless integration with Magento, allowing payments to be processed via internet banking from the magento site.

## Installation
### Composer Installation

If you have installed Magento 2 using composer, you can use composer to download and install this module.

Navigate to the root your Magento 2 root directory
#### Example:
```
cd /var/www/magento
```

Run the following commands to download and install the module using composer
#### Example:
```
composer require sid-payment/module-instant-eft
php bin/magento module:enable SID_InstantEFT
php bin/magento setup:upgrade
php bin/magento cache:flush
```

### Manual Installation 

Create a directory in <your Magento 2 root directory>/app/code called SID

#### Example:
```
mkdir /var/www/magento/app/code/SID
```
Copy this InstantEFT directory to the newly created directory

#### Example:
```
cp -R ~/SID /var/www/magento/app/code/SID
```

Run the setup commands from your Magento 2 root directory

#### Example:
```
php bin/magento module:enable SID_InstantEFT
php bin/magento setup:upgrade
php bin/magento cache:flush
```

You can check if the module has been installed using `bin/magento module:status`

You should be able to see `SID_InstantEFT` in the enabled module list

## Module Configuration

Login to your Magento 2 admin site and navigate to `Stores -> Configuration -> Payment Method -> SID Instant EFT` to configure the payment module

Enter set Enabled to Yes

Enter he title that you wish customers to see when they are selecting a payment method for their order

Enter your SID details in the remaining fields

Click on the `Save Config` button

## Admin Usage

The following menu item will be added to the admin site
Sales -> SID Instant EFT Payments

This will list payments which have been processed via SID Instant EFT

### Support

Visit [http://sidpayment.com/getting-started/](http://sidpayment.com/getting-started) for support requests or email support@sidpayment.com.
